## Aurora - A lightweight PHP framework

Aurora is a small yet quiet feature-rich framework for PHP. It started out as a
framework to support the development of a private project, but since it was set
up quiet generic, I believed it could be used for a wide range of other
applications as well, so I decided to release the source of the framework.

This project borrows a lot of functionality from existing frameworks and libraries
out there and combines the features we needed in a more streamlined and easy to
use way.

## Code examples

Stay true to the web with RESTful routing:

	'GET /' => function()
	{
	    return View::make('home/index');
	}

Redirect to a named route and flash something to the session:

	return Redirect::to_profile()->with('message', 'Welcome back!');

Retrieve a blog post and eagerly load the comments using Storm ORM:

	$posts = Post::with('comments')->find(1);

Get input from the previous request to re-populate a form:

	echo Input::old('email');

### Ready to learn more?

We will soon prepare the documentation.
