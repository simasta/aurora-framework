<?php

class RouterTest extends PHPUnit_Framework_TestCase
{
	/**
	 * De stubbed route loader.
	 *
	 * @var Loader
	 */
	public $loader;

	public function setUp()
	{
		// Maak een array van gevarieerde routes om mee te testen.
		$routes = array(
			'GET /'                         => array('name' => 'root', 'do' => function() {return 'root';}),
			'GET /home'                     => array('name' => 'home', 'do' => function() {}),
			'POST /home'                    => array('name' => 'post-home', 'do' => function() {}),
			'GET /user/(:num)'              => array('name' => 'user', 'do' => function() {}),
			'GET /cart/(:num?)'             => array('name' => 'cart', 'do' => function() {}),
			'GET /download/(:num?)/(:any?)' => array('name' => 'download', 'do' => function() {}),
			'GET /user/(:any)/(:num)/edit'  => array('name' => 'edit', 'do' => function() {})
		);

		// Maak een stub van de Route Loader.
		$this->loader = $this->getMock('System\\Routing\\Loader', array(), array(APP_PATH));
		$this->loader->expects($this->any())->method('load')->will($this->returnValue($routes));
	}

	public function testReturnsNullWhenNotFound()
	{
		$this->loader->expects($this->any())->method('load')->will($this->returnValue(array()));
		$this->assertNull(System\Routing\Router::make('GET', 'test', $this->loader)->route());
	}

	public function testRoutesToRoot()
	{
		$this->assertEquals(System\Routing\Router::make('GET', '/', $this->loader)->route()->callback['name'], 'root');
	}

	/**
	 * @dataProvider routeSegmentProvider
	 */
	public function testRoutesWhenSegmentsArePresent($method, $uri, $name)
	{
		$this->assertEquals(System\Routing\Router::make($method, $uri, $this->loader)->route()->callback['name'], $name);
	}

	public function routeSegmentProvider()
	{
		return array(
			array('GET', 'home', 'home'),
			array('GET', 'user/1', 'user'),
			array('GET', 'user/bart/22/edit', 'edit'),
			array('POST', 'home', 'post-home'),
		);
	}

	public function testParsesSegmentsIntoParameters()
	{
		// Test met één enkele parameter.
		$this->assertEquals(System\Routing\Router::make('GET', 'user/1', $this->loader)->route()->parameters[0], 1);
		$this->assertEquals(count(System\Routing\Router::make('GET', 'user/1', $this->loader)->route()->parameters), 1);

		// Test met twee parameters.
		$this->assertEquals(System\Routing\Router::make('GET', 'user/bart/21/edit', $this->loader)->route()->parameters[1], 21);
		$this->assertEquals(System\Routing\Router::make('GET', 'user/bart/21/edit', $this->loader)->route()->parameters[0], 'bart');
		$this->assertEquals(count(System\Routing\Router::make('GET', 'user/bart/21/edit', $this->loader)->route()->parameters), 2);

		// Test met optionele parameters (één en twee).
		$this->assertEquals(System\Routing\Router::make('GET', 'cart/1', $this->loader)->route()->parameters[0], 1);
		$this->assertEquals(count(System\Routing\Router::make('GET', 'cart/1', $this->loader)->route()->parameters), 1);

		$this->assertEquals(System\Routing\Router::make('GET', 'download/1', $this->loader)->route()->parameters[0], 1);
		$this->assertEquals(count(System\Routing\Router::make('GET', 'download/1', $this->loader)->route()->parameters), 1);

		$this->assertEquals(System\Routing\Router::make('GET', 'download/1/a', $this->loader)->route()->parameters[0], 1);
		$this->assertEquals(System\Routing\Router::make('GET', 'download/1/a', $this->loader)->route()->parameters[1], 'a');
		$this->assertEquals(count(System\Routing\Router::make('GET', 'download/1/a', $this->loader)->route()->parameters), 2);
	}

	public function testRoutesWhenUsingOptionalSegments()
	{
		$this->assertEquals(System\Routing\Router::make('GET', 'cart', $this->loader)->route()->callback['name'], 'cart');
		$this->assertEquals(System\Routing\Router::make('GET', 'cart/1', $this->loader)->route()->callback['name'], 'cart');
		$this->assertEquals(System\Routing\Router::make('GET', 'download', $this->loader)->route()->callback['name'], 'download');
		$this->assertEquals(System\Routing\Router::make('GET', 'download/1', $this->loader)->route()->callback['name'], 'download');
		$this->assertEquals(System\Routing\Router::make('GET', 'download/1/a', $this->loader)->route()->callback['name'], 'download');

		// The router moet null returnen als een optionele route parameter niet met de request URI matched.
		$this->assertNull(System\Routing\Router::make('GET', 'cart/a', $this->loader)->route());
		$this->assertNull(System\Routing\Router::make('GET', 'cart/1/a', $this->loader)->route());
		$this->assertNull(System\Routing\Router::make('GET', 'download/a/1', $this->loader)->route());
		$this->assertNull(System\Routing\Router::make('GET', 'download/1/a/c', $this->loader)->route());
	}
}
