<?php

class Utils
{
	/**
	 * Stel de default database connectie in als de fixture database.
	 */
	public static function setup_db()
	{
		$connections = array(
			'sqlite' => array(
				'driver' => 'sqlite',
				'database' => FIXTURE_PATH.'fixture.sqlite'
			)
		);

		System\Config::set('db.connections', $connections);
	}
}