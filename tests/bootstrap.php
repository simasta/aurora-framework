<?php
/**
 * Unit Tests voor het Aurora PHP framework.
 *
 * @package  Aurora
 * @version  1.2.0
 * @author   Bart Willemsen
 * @link     http://www.bart-willemsen.nl
 */

// Definieer de core framework paden.
define('APP_PATH', realpath('../application').'/');
define('BASE_PATH', realpath('../').'/');
define('PUBLIC_PATH', realpath('../public').'/');
define('SYS_PATH', realpath('../system').'/');

// Definieer diverse andere framework paden.
define('CACHE_PATH', APP_PATH.'storage/cache/');
define('CONFIG_PATH', APP_PATH.'config/');
define('DATABASE_PATH', APP_PATH.'storage/db/');
define('LANG_PATH', APP_PATH.'lang/');
define('LIBRARY_PATH', BASE_PATH.'libraries/');
define('MODEL_PATH', APP_PATH.'models/');
define('PACKAGE_PATH', BASE_PATH.'packages/');
define('ROUTE_PATH', APP_PATH.'routes/');
define('SESSION_PATH', APP_PATH.'storage/sessions/');
define('VIEW_PATH', APP_PATH.'views/');

// Definieer het fixture pad.
define('FIXTURE_PATH', realpath('fixtures').'/');
define('MODULE_PATH', FIXTURE_PATH.'modules/');

// Defineer de PHP file extentie.
define('EXT', '.php');

// Definieer de actieve module.
define('ACTIVE_MODULE', 'application');

// Laad de classes die gebruikt worden door de auto-loader.
require SYS_PATH.'config'.EXT;
require SYS_PATH.'arr'.EXT;

// Registreer de auto-loader.
require SYS_PATH.'loader'.EXT;
spl_autoload_register(array('System\\Loader', 'load'));
System\Loader::bootstrap();

// Laad de test utilities.
require 'utils'.EXT;