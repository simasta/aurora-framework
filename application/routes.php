<?php

return array(

    /*
	 * Applicatie Routes
	 *--------------------------------------------------------------------------
	 *
	 * Dit is de publieke API van je applicatie. Om functionaliteit toe te voegen
     * aan je applicatie, voeg je gewoon toe aan de array in dit bestand.
	 *
	 * Vertel Aurora gewoon de HTTP verbs en request URIs waar hetop moet reageren.
	 * Je mag op de GET, POST, PUT, of DELETE verbs reageren. Geniet van de simpele
	 * en elegantie van RESTful routing.
	 *
	 * Zo reageer je op een simpele GET request naar http://example.com/hallo
	 *
	 *      'GET /hello' => function()
	 *      {
	 *          return 'Hello World!';
	 *      }
	 *
	 * Je kan zelfs op meer dan 1 URI reageren:
	 *
	 *      'GET /hello, GET /world' => function()
	 *      {
	 *          return 'Hello World!';
	 *      }
	 *
	 * Het is makkelijk URI wildcards toe te staan met de (:num) en (:any) placeholders:
	 *
	 *      'GET /hello/(:any)' => function($name)
	 *      {
	 *          return "Welkom, $name";
	 *      }
	 */

	'GET /' => function()
	{
		return View::make('home.index');
	},
);