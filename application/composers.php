<?php

return array(

	/*
	 * View Namen & Composers
	 *--------------------------------------------------------------------------
	 * Named views geven een mooie syntax wanneer je met je views werkt.
	 *
	 * Zo definieer je een named view:
	 *
	 *      home.index => array('name => 'home')
	 *
	 * Nu kun je een instance van de view maken met de View::of dynamic
	 * method. Zie hier het voorbeeld:
	 *
	 *      return View::of_layout();
	 *
	 * View composers bieden een makkelijke manier om vaak voorkomende elementen
	 * aan de view toe te voegen wanneer hij gemaakt wordt. Bijvoorbeeld, als je
	 * elke keer als hij gemaakt wordt een header en footer partial wil toevoegen.
	 *
	 * De composer krijgt een instance van de view die gemaakt wordt, en is vrij
	 * om je view aan te passen hoe je maar wil. Zo definieer je er ��n:
	 *
	 *      home.index => function($view)
	 *      {
	 *          //
	 *      }
	 *
	 * Natuurlijk kun je een view name en composer voor ��n view maken:
	 *
	 *      'home.index' => array('name' => 'home', function($view)
	 *      {
	 *          //
	 *      })
	 */

	'home.index' => array('name' => 'home', function($view)
	{
		//
	}),
);