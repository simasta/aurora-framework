<?php

return array(

	/*
	 * Filters
	 *--------------------------------------------------------------------------
	 *
	 * Filters bieden een makkelijke manier om functionaliteit aan je routes
	 * toe te voegen. Filters kunnen zowel voor als na een route uitgevoerd is
	 * gedraaid worden.
	 *
	 * De ingebouwde "before" en "after" filters worden voor en na elke request
	 * naar je applicatie uitgevoerd; maar, je kan andere filters maken dat aan
	 * losse routes toegekend kunnen worden.
	 *
	 * Filters maken ook allerlei taken als authenticatie en CSRF protectie super
	 * eenvoudig. Als een filter dat voor een route draait een response returnt,
	 * override die response de route action.
	 *
	 * Laten we door een voorbeeld heen lopen..
	 *
	 * definieer eerst een filter:
	 *
	 *      'simple_filter' => function()
	 *      {
	 *          return 'Filtered!';
	 *      }
	 *
	 * Koppel de filter nu aan een route:
	 *
	 *      'GET /' => array('before' => 'simple_filter', function()
	 *      {
	 *          return 'Hello World!';
	 *      })
	 *
	 * Nu zal elke request naar http://example.com 'Filtered!' returnen omdat
	 * de filter de route action override door een waarde te returnen.
	 *
	 * Om je lever makkelijker te maken, hebben we authenticatie en CSRF
	 * filters gemaakt die klaar zijn om gekoppeld te worden aan routes.
	 */

	'before' => function()
	{
		// Doe iets voor elke request wordt uitgevoerd.
	},

	'after' => function($response)
	{
		// Doe iets nadat elke request is uitgevoerd.
	},

	'auth' => function()
	{
		return (! Auth::check()) ? Redirect::to_login() : null;
	},

	'csrf' => function()
	{
		return (Input::get('csrf_token') !== Form::raw_token()) ? Response::error('500') : null;
	},
);