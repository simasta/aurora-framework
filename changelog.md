# Aurora Change Log

## Version 1.4.8

- Fixed crash in Route loader.

### Upgrading from 1.4.7

- Replace **system** directory.

## Version 1.4.7

- Made framework work with PHP7 by updating some incompatible code in PHPass.
- Fixed bug in Storm causing IDs to overlap.

### Upgrading from 1.4.6

- Replace **system** directory.

## Version 1.4.6

- Fixed bug in Form class that caused name attributes to be overwritten.

### Upgrading from 1.4.5

- Replace **system** directory.

## Version 1.4.5

- Fixed bug that prevented view composers from being called for module named views.

### Upgrading from 1.4.4

- Replace **system** directory.

## Version 1.4.4

- Fix bug that caused a white screen when rendering a view that doesn't exist.

### Upgrading from 1.4.3

- Replace **system directory